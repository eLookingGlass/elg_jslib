// NOTE: does not work properly with values longer than
Number.prototype.formatMoney = function(places, symbol, thousand, decimal) 
{
	places = !isNaN(places = Math.abs(places)) ? places : 2;
	symbol = symbol !== undefined ? symbol : "$";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var number = this, 
		negative_start = number < 0 ? "("+symbol : symbol,
		negative_end = number < 0 ? ")" : "",
		i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
	return negative_start + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "")+negative_end;
};
// https://www.thewebflash.com/toggling-fullscreen-mode-using-the-html5-fullscreen-api/

function toggleFullscreen(elem) 
{
   elem = elem || document.documentElement;
   
   if (!document.fullscreenElement       && !document.mozFullScreenElement &&
	   !document.webkitFullscreenElement && !document.msFullscreenElement) 
   {
	  if (elem.requestFullscreen) 
	  {
		 elem.requestFullscreen();
	  } 
	  else if (elem.msRequestFullscreen) 
	  {
		 elem.msRequestFullscreen();
	  } 
	  else if (elem.mozRequestFullScreen) 
	  {
		 elem.mozRequestFullScreen();
	  } 
	  else if (elem.webkitRequestFullscreen) 
	  {
		 elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
	  }
   } 
   else 
   {
	  if (document.exitFullscreen) 
	  {
		 document.exitFullscreen();
	  } 
	  else if (document.msExitFullscreen) 
	  {
		 document.msExitFullscreen();
	  }
	  else if (document.mozCancelFullScreen) 
	  {
		 document.mozCancelFullScreen();
	  }
	  else if (document.webkitExitFullscreen) 
	  {
		 document.webkitExitFullscreen();
	  }
   }
}

document.getElementById('btn-fullscreen').addEventListener('click', function() {
  toggleFullscreen();
});

var themeColors = "theme-white theme-purple theme-green theme-dark";

$(document).ready(function () {
    $('.theme-blue').click(function () {
        $('body').removeClass(themeColors);
    });
    $('.theme-purple').click(function () {
        $('body').removeClass(themeColors).addClass("theme-purple");
    });
    $('.theme-green').click(function () {
        $('body').removeClass(themeColors).addClass("theme-green");
    });
    $('.theme-dark').click(function () {
        $('body').removeClass(themeColors).addClass("theme-dark");
    });    
});

var headerColors = "header-white header-purple header-green header-dark header-blue header-lightgrey";

$(document).ready(function () {
    $('.header-lightgrey').click(function () {
        $('body').removeClass(headerColors).addClass("header-lightgrey");
    });
    $('.header-purple').click(function () {
        $('body').removeClass(headerColors).addClass("header-purple");
    });
    $('.header-green').click(function () {
        $('body').removeClass(headerColors).addClass("header-green");
    });
    $('.header-dark').click(function () {
        $('body').removeClass(headerColors).addClass("header-dark");
    });
    $('.header-blue').click(function () {
        $('body').removeClass(headerColors).addClass("header-blue");
    });  
});

var sidebarColors = "sidebar-white sidebar-purple sidebar-green sidebar-dark sidebar-blue";

$(document).ready(function () {
    $('.sidebar-blue').click(function () {
        $('body').removeClass(sidebarColors).addClass("sidebar-blue");
    });
    $('.sidebar-lightgrey').click(function () {
        $('body').removeClass(sidebarColors).addClass("sidebar-lightgrey");
    });
    $('.sidebar-purple').click(function () {
        $('body').removeClass(sidebarColors).addClass("sidebar-purple");
    });
    $('.sidebar-green').click(function () {
        $('body').removeClass(sidebarColors).addClass("sidebar-green");
    });
    $('.sidebar-dark').click(function () {
        $('body').removeClass(sidebarColors).addClass("sidebar-dark");
    });  
});

$(document).ready(function () {
    $('#fixed-header-check').click(function () {
        if ($(this).is(":checked")) {
            $("nav.navbar").removeClass("navbar-static-top").addClass("navbar-fixed-top");
        } else {
            $("nav.navbar").removeClass("navbar-fixed-top").addClass("navbar-static-top");
        }
    });
});

/* Change to Fixed Header */
$(document).ready(function () {
    $('#fixed-header-check').click(function () {
        if ($(this).is(":checked")) {
            $("nav.navbar").removeClass("navbar-static-top").addClass("navbar-fixed-top");
        } else {
            $("nav.navbar").removeClass("navbar-fixed-top").addClass("navbar-static-top");
        }
    });
});

/* Change to Fixed Footer */
$(document).ready(function () {
    $('#fixed-footer-check').click(function () {
        if ($(this).is(":checked")) {
            $("footer").addClass("fixed-footer");
        } else {
            $("footer").removeClass("fixed-footer");
        }
    });
});

/* slimScroll */
$(document).ready(function () {
// Enable/disable fixed sidebar
    $('#fixed-sidebar-check').click(function () {
        if (($('#fixed-sidebar-check').is(':checked')) && (!$('body').hasClass('mini-navbar'))) {
            $("body").addClass('fixed-sidebar');
            $('#sidebar-collapse').slimScroll({
                height: '100%',              
                railOpacity: 0.9
            });
        } else {
            $('#sidebar-collapse').slimscroll({destroy: true});
            $('#sidebar-collapse').attr('style', '');
            $("body").removeClass('fixed-sidebar');
        }
    });
    
    $('#fixed-header-check[value="option1"]').prop('checked', false);
    $('#fixed-footer-check[value="option2"]').prop('checked', false);
    $('#fixed-sidebar-check[value="option3"]').prop('checked', false);

});

function swapStyleSheet(sheet){
	document.getElementById('theme-swap').setAttribute('href', sheet);
}
